# Implementación de Celda de Baucke

Repositorio con cuadernos de Jupyter y cuadernos Rmarkdown utilizados para los análisis de datos relacionados con el trabajo final de graduación de Ingeniería Física del Tecnológico de Costa Rica: Implementación de un método secundario para la medición potenciométrica de la actividad de iones hidronio a través de una celda diferencial de Baucke en la producción de materiales de referencia certificados.

Los cuadernos se utilizaron para documentar los análisis y respaldar el trabajo realizado durante la pasantía y el desarrollo del trabajo final de graduación.
