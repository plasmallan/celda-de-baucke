---
title: "Estimación de la incertidumbre por el método de Monte Carlo"
output:
  html_document:
    df_print: paged
---

```{r}
# install.packages("tidyverse")
# install.packages("readxl")
library(readxl)
library(tidyverse)
```
```{r}
N=10^6
```

La determinación del pH de una disolución utilizando una celda secundaria sigue el modelo de medición:

$$pH(S_1)=pH(MRC)-\frac{E_\Delta-E_j-E_{id}}{\ln{10}\left(\frac{R\left(T+273.15\right)}{F}\right)}+\delta_{rep}$$

Donde:

- $pH(MRC)$ es el $pH$ del material de referencia certificado.
- $E_j$ es el potencial de la unión líquida.
- $E_{id}$ es el potencial en el material de referencia certificado.
- $E_\Delta$ es el potencial entre el material de referencia certificado y el material de referencia que se está produciendo.
- $R$ es la constante de los gases ideales.
- $F$ es la constante de Faraday.
- $T$ es la temperatura del baño medida con la RTD.
- $\delta_{rep}=0$ es el término de la repetibilidad del $pH$ del baño.

El diagrama de Ishikawa muestra las fuentes de incertidumbre para cada magnitud:

<img src="Figuras/Imagen1.png" alt="drawing" width="1000"/>

Para $E_\Delta$ se tiene que:

$$U_{cal}=0.000\,000\,82\textrm{ V},\quad k=2$$

$$E_{cab}=\pm12\times10^{-6}\textrm{ V}$$

$$EMP_{inst}=\pm\frac{(19\cdot\left|rdg\right|+9\cdot0.2)\cdot\left|rdg\right|}{10^6}$$

$$d=10^{-8}\textrm{ V}$$

$$C=-0.000\,000\,19\textrm{ V}$$

Para $E_{id}$ se tiene que:

$$U_{cal}=0.000\,000\,82\textrm{ V},\quad k=2$$

$$E_{cab}=\pm12\times10^{-6}\textrm{ V}$$

$$EMP_{inst}=\pm\frac{(19\cdot\left|rdg\right|+9\cdot0.2)\cdot\left|rdg\right|}{10^6}$$

$$d=10^{-8}\textrm{ V}$$

$$C=-0.000\,000\,19\textrm{ V}$$

$$u_{rep}=\frac{s_{E_{id}}}{\sqrt{n}}$$

Para $E_j$ se tiene que:

$$E_j=0.1\cdot E_\Delta$$

$$u(E_j)=\frac{E_j}{\sqrt{12}}$$

Para $T$:

$$U_{cal}=0.024\textrm{ °C},\quad k=2$$

$$d=0.001\textrm{ °C}$$

$$C=+0.054\textrm{ °C}$$

Para $pH(MRC)$:

$$pH(MRC)=4.00$$

$$U_{MRC}=0.004,\quad k=2$$

Para $\delta_{rep}$

$$\delta_{rep}=0$$
$$u_{rep}=\frac{s_{pH(E_\Delta})}{\sqrt{n}}$$

Cuando $E_{id}<10\textrm{ $\mu$V}$, se toma $E_{id}$ como parte de la incertidumbre, siguiendo una distribución rectangular.

# Importación de datos

```{r}
file="C:/Users/allan/Documents/Repositories/celda-de-baucke/Datos/Validación de formulario/Certificación Sigma Aldrich - pH4.xlsm"
hojadelta=read_excel(file,sheet="Data E(Δ)",skip=12,col_names=TRUE)
hojaid=read_excel(file,sheet="Data E(id)",skip=12,col_names=TRUE)
```
```{r}
e_delta=hojadelta$`Potencial (V)`
e_id=hojaid$`Potencial (V)`
rep=hojadelta$`pHRTD (1)`
t=hojadelta$`RTD  (°C)`
```
# Modelo de medición
```{r}
pH=function(MRC,eD,eJ,eId,t,d){
  R=8.31446261815324
  F=96485.33212331
  return(MRC-(eD-eJ-eId)/(log(10)*(R*(t+273.15)/F))+d)
}
```
# Muestreos aleatorios
```{r}
eD=mean(e_delta)+rnorm(N,-0.00000019,0.00000082/2)+runif(N,-0.0000005,0.0000005)+
  runif(N,-(19*abs(mean(e_delta))+9*0.2)*abs(mean(e_delta))/10**6,(19*abs(mean(e_delta))+9*0.2)*abs(mean(e_delta))/10**6)+runif(N,-0.00000001/2,0.00000001/2)
```
```{r}
hist(eD)
```
```{r}
print(c(mean(eD),sd(eD)))
```
```{r}
eId=rnorm(N,mean(e_id),sd(e_id)/sqrt(length(e_id)))+rnorm(N,-0.00000019,0.00000082/2)+runif(N,-0.0000005,0.0000005)+
  runif(N,-(19*abs(mean(e_id))+9*0.2)*abs(mean(e_id))/10**6,(19*abs(mean(e_id))+9*0.2)*abs(mean(e_id))/10**6)+runif(N,-0.00000001/2,0.00000001/2)
```
```{r}
hist(eId)
```
```{r}
print(c(mean(eId),sd(eId)))
```